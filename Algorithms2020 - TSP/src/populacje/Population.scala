package populacje

import scala.util.Random
import Params._

class Population( val task: Task, solutions: List[ Solution], pars: settableParams) extends Serializable {
    var population = solutions.map( s=>if( s.path == null) s.setRandom else s)
    val time = System.nanoTime()

    def getWorst = population.reduce( (a,b) => (a max b))
    def getBest =  population.reduce( (a,b) => (a min b))
    
    var localBest = getBest

    def replaceWorse( solution: Solution) = {
        var i = 0
        var res = false
        do 
            if( solution.fitness < population( i).fitness) {
                population = population.updated( i, solution)
                i = population.size
                localBest  = localBest min solution
                res = true
            } else i = i+1
        while( i < population.size)
        res
    }

    def randOptimizationAlgorithm1: Solution=>Solution = oAlgs1( Random.nextInt( oAlgs1.size))
    def randOptimizationAlgorithm2: (Solution,Solution)=>Solution = oAlgs2( Random.nextInt( oAlgs2.size))

    def applyAlg( solution: Solution)  = {
        var s = solution
        if( Random.nextInt( oAlgs1.size+oAlgs2.size) < oAlgs1.size) s =  randOptimizationAlgorithm1( s)
        else {
            val r = population( Random.nextInt( population.size))
            if( s.fitness != r.fitness) s = randOptimizationAlgorithm2( s max r, s min r)
        }
        s
    }
    
    def localSearch = {
        while( (System.nanoTime - time)/100000000 < pars.searchTime) {
            val oldS = population( Random.nextInt( population.size))
            val newS = applyAlg( oldS)
            if( newS.fitness < oldS.fitness) replaceWorse( newS)
        }
        population
    }

    override def toString = 
        population.sortBy( _.fitness).map( a=>" "+a.fitness).toString  
}

