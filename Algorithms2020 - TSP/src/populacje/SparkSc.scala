package populacje

import org.apache.spark._
import org.apache.spark.broadcast._
import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}

class SparkSc extends Serializable {
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)

    def getContext( isLocal: Boolean): SparkContext = {
        @transient lazy val conf: SparkConf = 
              if( isLocal) new SparkConf().setMaster("local").setAppName("tsp")
              else new SparkConf().setAppName("tsp")
    
        @transient lazy val sc: SparkContext = new SparkContext( conf)
        sc.setLogLevel("OFF")
        sc
    }
}

