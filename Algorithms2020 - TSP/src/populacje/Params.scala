package populacje

import scala.util.Random

class settableParams extends Serializable{
    var task =  
          // ----- euclidean, round ----- 
          "pr76"          //108159     
          //"kroA100"       //21282      
          //"pr1002"        //259045      
          //"pcb442"        //50778      
          //"a280"          //2579       
          //"ch150"         //6528       
          //"tsp225"        //3916
    

    var runsNo = 10             //number of program runs

    var searchTime = 2          //time in ds
    var populationsNo = 10       //number of populations (one population per thread)
    var populationSize = 2
    var maxWithoutChange = 2    //the process terminates when the best solution doesn't change in maxWithoutChange rounds
                                //when 1 - the process terminates when the best solution in the next round is the same as in the previous 
    var isLocal = true          //local or cluster???

    var dir = ""
    var filename = ""
    
    def printParams = {
        println( "Task = "+task)
        println( 
            "runsNo = "+runsNo+", "+
            "populationsNo = "+populationsNo+", "+
            "populationSize = "+populationSize+", "+
            "searchTime = "+searchTime+", "+
            "maxWithoutChange = "+maxWithoutChange
        )
    }
    
    def setDir {
        dir = if( isLocal) "e:/dane/"  else "/home/iza/TSP/"
        filename = dir+task+".tsp" 
    }
    
}


object Params {

    val oAlgs1: Array[Solution => Solution] = Array(   
                          Agents.opti1RandomMove _,
                          Agents.opti1BestMove _,
                          Agents.opti1RandomReverse _,
                          Agents.opti1BestReverse _
                       )  //agents that take one solution as parameter
     
    val oAlgs2: Array[(Solution, Solution) => Solution] = Array(
                          Agents.opti2Cross _
                       )  //agents that take two solutions as parameter
     
                       
                       
    //TSP
    def nextI( i: Int, size: Int) = (i+1)%size
    def prevI( i: Int, size: Int) = (i+size-1)%size

    val probabilities = Array( 0.8, 0.9, 1)
    def randI( maxI: Int) = {
        var r = Random.nextDouble
        if( r < probabilities( 0))  0
        else if( (r < probabilities( 1)) && (maxI > 0))  1
        else {
            if( maxI > 2) Random.nextInt( maxI-2)+2
            else maxI
        }
    }

    val kNearestsForRandom = 5   //number of nearest nodes from which next node in path is drawn
    
    //test parameters
    val testSolutions = true    //test all solutions?

    val algStatistics = true
    var algsStats: Map[ String, Int] = Map() 
}




