package populacje

import scala.util.Random
import Params._

object Agents {

    def checkChange( oldFitness: Double, newFitness: Double, algName: String) = {
    // if algName improved the fitness, adds its name to statistics
        if(( newFitness < oldFitness) && Params.algStatistics)
            if( Params.algsStats.contains( algName)) Params.algsStats = Params.algsStats + (algName -> (Params.algsStats( algName)+1))
            else Params.algsStats = Params.algsStats + (algName -> 1)
    }
    
    def opti1RandomReverse( solution: Solution) = {
        val oldFitness = solution.fitness
        val newSolution = solution.randomReverse
        checkChange( oldFitness, newSolution.fitness, "opti1RandomReverse") 
        solution
    }
    
    def opti1BestReverse( solution: Solution) = {
        val oldFitness = solution.fitness
        val newSolution = solution.bestReverse
        checkChange( oldFitness, newSolution.fitness, "opti1BestReverse")
        newSolution
    }
    
    def opti1BestMove( solution: Solution) = {
        val oldFitness = solution.fitness
        var node: Int = Random.nextInt( solution.size)
        val newSolution = solution.bestMove( node)
        checkChange( oldFitness, newSolution.fitness, "opti1BestMove")
        newSolution
    }
    
    def opti1RandomMove( solution: Solution) = {
        val oldFitness = solution.fitness
        var node: Int = Random.nextInt( solution.size)
        val newSolution = solution.randomMove( node)
        checkChange( oldFitness, newSolution.fitness, "opti1RandomMove")
        newSolution
    }
    
    def opti2Cross( worse: Solution, better: Solution) = {
        val oldFitness = worse.fitness
        val lBest = worse.copy.cross( better)
        if( lBest.fitness == better.fitness) worse
        else {
            checkChange( oldFitness, lBest.fitness, "opti2Cross")
            lBest
        }
    }
    
}


