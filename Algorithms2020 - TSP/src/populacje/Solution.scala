package populacje

import scala.util.Random

@throws(classOf[Exception])
class Solution( val task: Task) extends Serializable {
  
  
    var size = task.points.size
    var path: List[ Int] = null
    var fitness: Double = Double.MaxValue
    
    def this( task: Task, path: List[ Int], fitness: Double) = {
        this( task)
        this.path = path
        this.fitness = fitness
    }
    
    def nextI( i: Int) = (i+1)%size
    def prevI( i: Int) = (i+size-1)%size

//------------------- paths
    
    def setRandom = {
        def findNearests( node: Int, nodes: List[ Int]) = {
            var distances = nodes.map( a => (a, task.distance( node, a)))
            var nrsts: List[ Int] = List()
            for(a <- 1 to Math.min( Params.kNearestsForRandom, nodes.size)) {
                val m = distances.minBy( _._2)
                nrsts  = nrsts  :::  List( m._1)
                distances = distances diff List( m)  
            }
            nrsts
        }
    
        def randI( maxI: Int) = {
            var r = Random.nextDouble
            if( r < Params.probabilities( 0))  0
            else if( (r < Params.probabilities( 1)) && (maxI > 0))  1
            else {
                if( maxI > 2) Random.nextInt( maxI-2)+2
                else maxI
            }
        }
        
        var last = Random.nextInt( size-1)
        var all = (0 to last-1).toList ::: (last+1 to size-1).toList
        path = List( last)
        while( path.size < size) {
            val rnd = findNearests( last, all)
            last = rnd( randI( rnd.size-1))
            path = path ::: List( last)
            all = all diff List( last) 
        }
        countFitness
    }
   
//-------------------

    def shift0 = {
        if( path( 0) != 0) {
            val where0 = path.indexOf( 0)
            path = path.slice( where0, size) ::: path.slice( 0, where0)
        }
    }

    def countFitness = {
        shift0  
        this.fitness = task.pathLength( path)
        this
    }

    def copy: Solution = new Solution( task, path, fitness)
    
    def min (s: Solution) = {
        if( this.fitness <= s.fitness) this.copy
        else s.copy
    }

    def max (s: Solution) = {
        if( this.fitness > s.fitness) this.copy
        else s.copy
    }

    
//-------------------
    
    def reverse( from:Int, len:Int) = {
        var change = 0.0
        change -= task.distance( path( prevI( from)), path( from))
        change -= task.distance( path( prevI( from+len)), path( from+len))
        change += task.distance( path( prevI( from)), path( from+len-1))
        change += task.distance( path( from), path( from+len))
        
        if( change < 0) {
            var slice = path.slice( from, from + len)
            slice = slice.reverse
            path = path.slice( 0, from) ::: slice ::: path.slice( from+len, path.size)
            fitness = fitness + change
            shift0
            if( Params.testSolutions) this.test( "reverse ") 
        }
        this
    }
    
    def randomReverse = {
        var len: Int = Random.nextInt( size-2)+2
        val from = Random.nextInt( size - len)
        reverse( from, len)
    }
    
    def bestReverse = {
        var bestChange:Double = 0
        var bestFrom = -1
        var bestLen = -1
        for( len <- (2 to size))
            for( from <- (0 to size-len-1)) {
                var change = 0.0
                change -= task.distance( path( prevI( from)), path( from))
                change -= task.distance( path( prevI( from+len)), path( from+len))
                change += task.distance( path( prevI( from)), path( from+len-1))
                change += task.distance( path( from), path( from+len))
                if( change < bestChange) {
                    bestChange = change
                    bestFrom = from 
                    bestLen = len
                }
            }
        if( bestChange < 0) {
            var slice = path.slice( bestFrom, bestFrom + bestLen)
            slice = slice.reverse
            path = path.slice( 0, bestFrom) ::: slice ::: path.slice( bestFrom+bestLen, size)
            fitness = fitness + bestChange
            shift0
            if( Params.testSolutions) this.test( "bestReverse ") 
        }
        this
    }

    def bestMove( node: Int) = {
        val deleted = path( node)
        var deletionChange = task.distance( path( prevI( node)), path( nextI( node)))-
                             task.distance( path( prevI( node)), deleted)-
                             task.distance( deleted, path( nextI( node)))
                             
        var pathWithoutNode = path.slice( 0, node) ::: path.slice( node+1, size)
        var bestInsertionChange = Long.MaxValue
        var bestI = -1
        var start = Random.nextInt( size-1)  
        for( j <- (0 until size-1)) {
            var i = (j+start)%(size-1)
            if( i != node-1) {
                val change = -task.distance( pathWithoutNode( i), pathWithoutNode( ( i+1)%(size-1)))+
                              task.distance( pathWithoutNode( i), deleted)+
                              task.distance( deleted, pathWithoutNode( ( i+1)%(size-1)))
                if( change < bestInsertionChange) {
                    bestInsertionChange = change
                    bestI = i
                }
            }
        }
        if( bestInsertionChange + deletionChange < 0) {
            path = pathWithoutNode.slice( 0, bestI+1) ::: List( deleted) ::: 
                   pathWithoutNode.slice( bestI+1, size)
            fitness = fitness +(deletionChange +bestInsertionChange)
            shift0
            if( Params.testSolutions) this.test( "bestMove ") 
        }
        this
    }

    def randomMove( node: Int) = {
        val deleted = path( node)
        var deletionChange = task.distance( path( prevI( node)), path( nextI( node)))-
                             task.distance( path( prevI( node)), deleted)-
                             task.distance( deleted, path( nextI( node)))
                             
        var pathWithoutNode = path.slice( 0, node) ::: path.slice( node+1, size)
        var i = Random.nextInt( size-1)
        while( i == node-1) i = Random.nextInt( size-1)   
        val insertionChange = -task.distance( pathWithoutNode( i), pathWithoutNode( ( i+1)%(size-1)))+
                      task.distance( pathWithoutNode( i), deleted)+
                      task.distance( deleted, pathWithoutNode( ( i+1)%(size-1)))
        if( insertionChange + deletionChange < 0) {
            path = pathWithoutNode.slice( 0, i+1) ::: List( deleted) ::: 
                   pathWithoutNode.slice( i+1, size)
            fitness = fitness +(deletionChange +insertionChange)
            shift0
            if( Params.testSolutions) this.test( "randomMove ") 
        }
        this
    }

  	def cross( solution: Solution)  = {
    		val from: Int = Random.nextInt( path.size)
    		val len : Int = Random.nextInt( path.size/2 - 2)+2
    		
    		val slice = path.slice( from, from+len)
        val rest = solution.path diff slice
        path = rest.slice( 0, from) ::: slice ::: rest.slice( from, rest.size)
        countFitness
    		if( Params.testSolutions) test( "oneRelinking ")
    		this
  	}
  
    def test( s: String = "") = {
        var err = ""
        if( (Math.abs( task.pathLength( path) - fitness)) > 0.0001) err += "fitness miscalculated: ma byc "+task.pathLength( path)+" != jest " + fitness
        if( path.size != size) err += "wrong size ("+ path.size + ")" 
        if( path.min < 0) println( s+"min < 0: " + path.min)
        if( path.max > size-1) err += "max > "+(size-1) + ": "+path.max
        if( path.distinct.size != size) {
            err += "nodes are not unique: " +path + "\n " +path.groupBy(identity).filter( _._2.size>1).map(t => (t._1 + " "+ t._2.size)).mkString( ",")
        }
        if( err != "") {
            println( s + err + " " + "\n"+this.path + "   "+ this.fitness)
            throw new Exception
        }
        this
    }

    override def toString =  ""+fitness + "->"+path.toString()

}


