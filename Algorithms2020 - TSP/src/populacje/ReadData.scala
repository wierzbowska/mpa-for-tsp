package populacje

import scala.io.Source

object ReadData {

    def read( filename: String) = {
        def trans( s: String): Option[(Double, Double)] = {
          if( s.size>0) {
            val ss = s.replaceAll("^\\s+", "")
            val pcs = ss.trim.split("\\s+")
            try Some( (pcs( 1).toDouble, pcs(2).toDouble))
            catch { case _: Exception => None }
          } else {
            println( "uwaga")
            None
          }
        }
    
        var points = Source.fromFile( filename).getLines.filter( !_.head.isLetter).flatMap( a=> trans( a)).toList
        points
    }
   
}
