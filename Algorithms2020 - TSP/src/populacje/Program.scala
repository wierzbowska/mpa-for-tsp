package populacje

import scala.util.Random

object Program extends SparkSc {
    var pars = new settableParams()
    
    def processArgs( args: List[String]): Boolean = {
        args match {
            case Nil => true           // everything processed
            case "-task" :: n :: rest => { 
                pars.task = n 
                processArgs( rest) 
            }   
            case "-isLocal" :: n :: rest => { pars.isLocal = ( n == "true"); processArgs( rest) }
            case "-runsNo" :: n :: rest => { pars.runsNo = n.toInt; processArgs( rest) }
            case "-populationSize" :: n :: rest => { pars.populationSize = n.toInt; processArgs( rest) }
            case "-populationsNo" :: n :: rest => { pars.populationsNo = n.toInt; processArgs( rest) }
            case "-maxWithoutChange" :: n :: rest => { pars.maxWithoutChange = n.toInt; processArgs( rest) }
            case "-searchTime" :: n :: rest => { pars.searchTime = n.toInt; processArgs( rest) }
            case _ => { println( args( 0)+" not processed"); false }           // some elements were not processed
        }
    }
    
    def main(args: Array[String]) = {
        if( processArgs( args.toList)) {
            pars.setDir
            pars.printParams
            val sc = getContext( pars.isLocal) 
            if( pars.task == "") println( "no task") 
            else {
                RunningTime.timeBy("...start...")

                var sumaWynikow = 0.0
                var no = 0
                for( run <- 1 to pars.runsNo) { 
                    val task = new Task( ReadData.read( pars.filename))
                    var population = new PopulationsRDD( sc, task, pars)
                    
                    var round = 1
                    var withoutChange = 0
                    while( (withoutChange < pars.maxWithoutChange) || (round <= 2)){
                        val oldBest = population.best.fitness
                        population = population.oneSearch
                        println( "round "+round + ": "  + population.solutions.map( s=>new java.text.DecimalFormat("#####").format( s.fitness)).mkString(","))
                        if( oldBest > population.best.fitness) withoutChange = 0 else withoutChange += 1
                        round = round+1
                    }
                    
                    val best = population.best.test()
                    sumaWynikow = sumaWynikow + population.best.fitness
                    no = no +1

                    println( "\nbest solution..."+best.fitness+"  suma="+sumaWynikow)
                    if( Params.algStatistics) println( "Statistics: "+Params.algsStats+"\n")
                    RunningTime.timeBy(run+"...end...")
                }
                println( "\n---------------------------------------------")
                println( "the sum of all results = "+sumaWynikow)
                println( "average result = "+(sumaWynikow/no))
            }
        }
    }         
}


