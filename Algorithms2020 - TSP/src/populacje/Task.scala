package populacje

import scala.io.Source
import scala.math._
import scala.util.Random

class Task( val points: List[(Double, Double)]) extends Serializable {

    def distanceXY( x: (Double, Double), y: (Double, Double)) = {
        //ceil( sqrt( pow( x._1-y._1, 2)+pow( x._2-y._2, 2)))          //euclidean, round up
        round( sqrt( pow( x._1-y._1, 2)+pow( x._2-y._2, 2)))           //euclidean

        /*
        //pseudo-euclidean
        val xd = x._1-y._1
        val yd = x._2-y._2
        val rij = sqrt( (xd*xd + yd*yd) / 10.0 )
        val tij = round( rij)
        if (tij<rij) tij + 1 else tij
        */
    }
    
    def distance( i: Int, j: Int) = distanceXY( points( i), points( j))   
    
    def distance( l: List[ Int]) = {
        distanceXY( points( l(0)), points( l(1)))
    }
    
    def pathLength( path: List[ Int]) = {
        path.sliding( 2).map( a=> distance( a)).sum + distance( path( 0), path( path.size-1))
    }

    def pathLengthNoCycle( path: List[ Int]) = {
        path.sliding( 2).map( a=> distance( a)).sum     
    }


}

