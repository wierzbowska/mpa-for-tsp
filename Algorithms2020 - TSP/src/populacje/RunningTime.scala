package populacje

object RunningTime {
  val time0 = System.nanoTime()
  var time = System.nanoTime()

  def timeToString( s: String, t: Long): String = {
      s + " "+t/1000/1000/1000 + "s"
  }

  def timeBy( s: String) {
    var t1 = System.nanoTime()
    println( timeToString( s, t1-time) + " (total"+timeToString( "", t1-time0)+")")
    time = t1
  }

  override def toString(): String = {
    var t1 = System.nanoTime()
    timeToString( "time", t1-time) + timeToString( "\ntotal time", t1-time0)
  }
}