package populacje

import org.apache.spark.rdd.RDD
import scala.util.Random
import org.apache.spark.SparkContext

class PopulationsRDD( sc: SparkContext, task: Task, pars: settableParams) {
  
    var total = pars.populationSize * pars.populationsNo
    
    var solutions = (0 until total).map( a=>(new Solution( task))).toList
    def getBest =  solutions.reduce( (a,b) => (a min b))
    var best: Solution = getBest
    
    def divide( ss: List[ Solution]) = {
        var population = Random.shuffle( ss ::: ((0 until total-ss.size).map( a=>(new Solution( task)))).toList)    
        (0 until pars.populationsNo).map( i => {
            val (left, right) = population.splitAt( pars.populationSize)
            population = right
            (i, left.toList)
        })
    }
    
    def oneSearch = {
        val lPars = pars
        val lTask = task
        var parallel = sc.parallelize( divide( solutions), pars.populationsNo)
        var impr = parallel.map( p => new Population( lTask, p._2, lPars).localSearch).flatMap( identity)
        solutions = impr.collect().toList   
        best = best min getBest
        this
    }
            
    override def toString = solutions.foldLeft("")( _+" "+_.fitness.toString)
  
}